// Import Dependencies
const express = require('express')
const bodyParser = require('body-parser')
const dotenv = require('dotenv').config()

const logger = require('morgan');
const cookieParser = require('cookie-parser');

const cors = require('cors');
global.mongoose = require('mongoose');

const mongodbUrl = process.env.MONGODB_URL  //'mongodb://13.232.136.152:27017/rental';
const opt = { useNewUrlParser: true, autoIndex: false, useUnifiedTopology: true }
global.mongoose.Promise = Promise;
global.ObjectId = global.mongoose.Types.ObjectId;
global.Schema = global.mongoose.Schema;
global.mongoose.connect(mongodbUrl, opt);

global.db = global.mongoose.connection;
global.db.on('error', (error) => {
    console.log('---FAILED APP to connect to mongoose', error)
});
global.db.once('open', () => {
    console.log('database connected')
});
// -------------------

// Initialise the app
const app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(cors());
// Import routes
const apiRoutes = require('./routes/index.route');
// Configure bodyparser to handle post requests
app.use(bodyParser.urlencoded({
    extended: true,
}));

app.use(bodyParser.json());
// // Send message for default URL
// app.get('/', (req, res) => res.send('Hello World with Express'));
app.use('', apiRoutes);

// Connect to Mongoose and set connection variable
const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log(`Running on port ${port}`);
});