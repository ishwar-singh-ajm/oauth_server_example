// currently not used 
const OAuthAuthorizationCodeSchema = new global.Schema({
    code: String,
    expiresAt: Date,
    redirectUri: String,
    scope: String,
    user: global.ObjectId,
    client: global.ObjectId,
},
    { timestamps: true, collection: 'oauthauthorizationcode' }
)

const OAuthAuthorizationCodeModel = global.db.model('OAuthAuthorizationCode', OAuthAuthorizationCodeSchema)

module.exports = { OAuthAuthorizationCodeModel, OAuthAuthorizationCodeSchema }