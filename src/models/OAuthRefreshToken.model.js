const OAuthRefreshTokenSchema = new global.Schema({
    refreshToken: String,
    refreshTokenExpiresAt: Date,
    scope: String,
    user: global.ObjectId,
    client: global.ObjectId,
},
    { timestamps: true, collection: 'oauthrefreshtoken' }
)

const OAuthRefreshTokenModel = global.db.model('OAuthRefreshToken', OAuthRefreshTokenSchema)

module.exports = { OAuthRefreshTokenModel, OAuthRefreshTokenSchema }