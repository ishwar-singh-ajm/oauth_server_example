// schema of User
const UserSchema = new global.Schema({
    fullName: String,
    emailAddress: String,
    password: String,
    scope: String
},
    { timestamps: true, collection: 'user' }
)

const UserModel = global.db.model('User', UserSchema)

module.exports = { UserModel, UserSchema }

// token: { type: String, trim: true },
// userId: { type: global.ObjectId, trim: true, default: null },
// storeId: { type: global.ObjectId, trim: true, default: null }