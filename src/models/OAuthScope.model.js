// currently not used 
const OAuthScopeSchema = new global.Schema({
    scope: String,
    is_default: Boolean
},
    { timestamps: true, collection: 'oauthscope' }
)

const OAuthScopeModel = global.db.model('OAuthScope', OAuthScopeSchema)

module.exports = { OAuthScopeModel, OAuthScopeSchema }