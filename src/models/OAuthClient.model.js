const OAuthClientSchema = new global.Schema({
    name: String,
    clientId: String,
    clientSecret: String,
    redirectUris: {
        type: [String]
    },
    grants: {
        type: [String],
        default: ['authorization_code', 'password', 'refresh_token', 'client_credentials']
    },
    scope: String,
    user: global.ObjectId
},
    { timestamps: true, collection: 'oauthclient' }
)

const OAuthClientModel = global.db.model('OAuthClient', OAuthClientSchema)

module.exports = { OAuthClientModel, OAuthClientSchema }