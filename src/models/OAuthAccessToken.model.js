// currently not used 
const OAuthAccessTokenSchema = new global.Schema({
    accessToken: String,
    accessTokenExpiresAt: Date,
    scope: String,
    user: global.ObjectId,
    client: global.ObjectId,
},
    { timestamps: true, collection: 'oauthaccesstoken' }
)

const OAuthAccessTokenModel = global.db.model('OAuthAccessToken', OAuthAccessTokenSchema)

module.exports = { OAuthAccessTokenModel, OAuthAccessTokenSchema }