const crypto = require('crypto')
/**
 * @desc Response boolean function
 * @param {key} code - The code of the response.
 * @returns {boolean} response object
*/
global.is_key_existed = function (key) {
  let flag = true
  if (key === undefined) {
    flag = false
  }
  return flag
}
exports.convertNumberToWords = (amountVal) => { }

exports.encrypt = function (data) {
  const key = process.env.ENCRYPTION_KEY
  const cipher = crypto.createCipher('aes-256-cbc', key)
  let crypted = cipher.update(data, 'utf-8', 'hex')
  crypted += cipher.final('hex')

  return crypted
}

exports.decrypt = function (data) {
  const key = process.env.ENCRYPTION_KEY
  const decipher = crypto.createDecipher('aes-256-cbc', key)
  let decrypted = decipher.update(data, 'hex', 'utf-8')
  decrypted += decipher.final('utf-8')

  return decrypted
}
