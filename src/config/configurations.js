exports.country = 'AU' // default is AU
exports.environment = 'LOCAL' // default is DEV
exports.region = 'us-east-1' // default is us-east-1
exports.language = 'en'
exports.attachmentFilePath = ''
exports.tempFolderPath = ''
exports.contentVersion = '1'
exports.releaseVersion = 'v1'
exports.releaseBotAlias = 'release'
exports.previewBotAlias = 'preview'

exports.organzationAwsAccountNumber = ''
exports.bucketPath = ''

/**
 * Set Content Version
 *
 * @param contentVersion
 */
exports.setContentVersion = (contentVersion) => {
  exports.contentVersion = contentVersion
}

/**
 * Set Attachment File Path
 *
 * @param intentName
 * @param file
 */
exports.setAttachmentFilePath = (intentName, file) => {
  const path = `${this.releaseVersion}/${this.country.toLowerCase()}/${this.appId}/${this.language}/${intentName}/${this.contentVersion}/${file}`
  exports.attachmentFilePath = path
}

/**
 * Set Temp Folder Path
 */
exports.setTempFolderPath = () => {
  exports.tempFolderPath = `${this.releaseVersion}/${this.country.toLowerCase()}/${this.appId}/${this.language}/`
}

/**
 * Set Country
 *
 * @param country
 */
exports.setCountry = (country) => {
  if (typeof (country) === 'string') {
    exports.country = country
  }
}

/**
 * Set appId
 *
 * @param appId
 */
exports.setAppId = (appId) => {
  if (typeof (appId) === 'string') {
    exports.appId = appId
  }
}

/**
 * Set Langauge
 *
 * @param lang
 */
exports.setLanguage = (lang) => {
  if (typeof (lang) === 'string') {
    exports.language = lang
  }
}

// // Set Environment
exports.setEnvironment = (environment) => {
  if (typeof (environment) === 'string') {
    exports.environment = environment
  }
}

/**
 * Print Console Log
 */

exports.printConsoleLog = (data) => {
  console.log(data)
}
