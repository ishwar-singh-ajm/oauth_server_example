const responseObj = {}
const bcrypt = require('bcrypt')
const { ObjectId } = require('mongodb')

const common = require('../config/common')
const errorCodes = require('../config/errorCodes')
const userRepository = require('./../repositories/user.repository')
const authRepository = require('./../repositories/auth.repository')
const language = require('../languages/language').init('english')

exports.login = async (request, response, next) => {
  const params = request.body
  const condition = {}
  condition.emailAddress = params.emailAddress
  userRepository.findUser(condition).then((userData) => {

    // check userData
    if (!userData) {
      responseObj.status = errorCodes.BAD_REQUEST
      responseObj.message = language.getMessage('INVALID_USER')
      responseObj.data = {}
      response.send(responseObj)
    }

    // compair password
    if (bcrypt.compareSync(params.password, userData.password)) {
      responseObj.status = errorCodes.BAD_REQUEST
      responseObj.message = language.getMessage('INVALID_EMAIL_OR_PASSWORD')
      responseObj.data = {}
      response.send(responseObj)
    }

  }).catch(function (error) {
    // in case of find user error
    responseObj.status = errorCodes.SYNTAX_ERROR
    responseObj.message = error
    responseObj.data = {}
    response.send(responseObj)
  })

  //   if (userData) {
  //     if (bcrypt.compareSync(params.password, userData.password)) {
  //         // Shop domain is found in params
  //         if (params.shopDomain && params.shopDomain !== undefined) {
  //             StoreRepository.findShop({ domain: params.shopDomain })
  //                 .then((shopData) => {
  //                     // update store id in user collection
  //                     const updateUserParam = {
  //                         storeId: ObjectId(shopData._id)
  //                     }

  //                     userRepository.updateUser({ _id: userData._id }, updateUserParam)
  //                         .then((updatedUserData) => {
  //                             const data = {}
  //                             data.authtoken = createJWT(userData._id)
  //                             data.userData = userData

  //                             // update user id in store
  //                             const updateStoreParam = {
  //                                 userId: ObjectId(userData._id)
  //                             }
  //                             StoreRepository
  //                                 .updateShop({ _id: shopData._id }, updateStoreParam)
  //                                 .then((updatedData) => {
  //                                 }).catch((error) => {
  //                                     // reject(error)
  //                                     responseObj.status = errorCodes.SYNTAX_ERROR
  //                                     responseObj.message = error.error_description
  //                                     responseObj.data = {}
  //                                     response.send(responseObj)
  //                                 })
  //                             responseObj.status = errorCodes.SUCCESS
  //                             responseObj.message = language.getMessage('SUCCESS')
  //                             responseObj.data = data
  //                             response.send(responseObj)
  //                         }).catch((updateUsrErr) => {
  //                             responseObj.status = errorCodes.SYNTAX_ERROR
  //                             responseObj.message = updateUsrErr.message
  //                             responseObj.data = {}
  //                             response.send(responseObj)
  //                         })
  //                 }).catch((error) => {
  //                     responseObj.status = errorCodes.SYNTAX_ERROR
  //                     responseObj.message = error.message
  //                     responseObj.data = {}
  //                     response.send(responseObj)
  //                 })
  //         } else {
  //             // Shop domain is not found then simple login and send response
  //             const data = {}
  //             data.authtoken = createJWT(userData._id)
  //             data.user = userData

  //             responseObj.status = errorCodes.SUCCESS
  //             responseObj.message = language.getMessage('SUCCESS')
  //             responseObj.data = data
  //             response.send(responseObj)
  //         }
  //     } else {
  //         // In case of password mismatch
  //         responseObj.status = errorCodes.BAD_REQUEST
  //         responseObj.message = language.getMessage('INVALID_EMAIL_OR_PASSWORD')
  //         responseObj.data = {}
  //         response.send(responseObj)
  //     }
  // } else {
  //     // in case of invalid user
  //     responseObj.status = errorCodes.BAD_REQUEST
  //     responseObj.message = language.getMessage('INVALID_USER')
  //     responseObj.data = {}
  //     response.send(responseObj)
  // }

}

exports.sendOTP = async (request, response, next) => {
  // const params = request.body
  // userRepository.login(params).then((result) => {
  //     response.send(result)
  // }).catch(function (error) {
  //     response.send(error)
  // })
}

exports.accessDenied = async (request, response, next) => {
  // response.send({ message: 'Access Denied' })
}

// fullName: Ishwar Singh Gehlot
// emailAddress: gehlotishwar@gmail.com
// password: ishwar
// scope: profile

exports.register = async (request, response, next) => {
  const params = request.body
  const condition = {}
  condition.emailAddress = params.emailAddress

  userRepository.findUser(condition).then((userData) => {
    // bcrypt password
    params.password = bcrypt.hashSync(params.password, 10)

    // Email already exists
    if (userData) {
      responseObj.status = errorCodes.BAD_REQUEST
      responseObj.message = language.getMessage('EMAIL_ALREADY_EXISTS')
      responseObj.data = {}
      response.send(responseObj)
      return;
    }

    // Create user data
    userRepository.createUser(params).then((userData) => {
      const data = {}
      //  create client
      data.authtoken = createJWT(userData._id)
      data.userData = userData
      responseObj.status = errorCodes.SUCCESS
      responseObj.message = language.getMessage('SUCCESS')
      responseObj.data = data
      response.send(responseObj)
    }).catch(function (error) {
      responseObj.status = errorCodes.SYNTAX_ERROR
      responseObj.message = error.message
      responseObj.data = {}
      response.send(responseObj)
    })


  })

}

exports.validateUser = async (request, response, next) => {
  // console.log(request)
  // next()
}

function createClient(user) {

}

function createJWT(userId) {
  // const secretKey = process.env.ENCRYPTION_KEY
  // var token = jwt.sign({ foo: userId }, secretKey, { expiresIn: '60' })

  // const params = {
  //     token: token,
  //     userId: userId
  // }

  // AuthRepository.create(params).then((response, error) => {
  //     if (response) {
  //         return response
  //     }
  // }).catch((error) => {
  //     if (error) {
  //         return error
  //     }
  // })

  // return token
}

/* function generateOTP () {
} */




// const condition = {}
  // condition.emailAddress = params.emailAddress

  // userRepository.findUser(condition).then((userData) => {
  //     params.password = bcrypt.hashSync(params.password, 10)
  //     if (!userData) {
  //         // Shp domain is found in params
  //         if (params.shopDomain && params.shopDomain !== undefined && params.shopDomain !== null) {
  //             // Find shop in shopify table
  //             StoreRepository.findShop({ domain: params.shopDomain }).then((shopData) => {
  //                 params.storeId = ObjectId(shopData._id)
  //                 // Add store id with other user data
  //                 userRepository.createUser(params).then((userData) => {
  //                     const data = {}
  //                     data.authtoken = createJWT(userData._id)
  //                     data.userData = userData

  //                     // update user id in store
  //                     const updateStoreParam = {
  //                         userId: ObjectId(userData._id)
  //                     }
  //                     StoreRepository
  //                         .updateShop({ _id: shopData._id }, updateStoreParam)
  //                         .then((updatedData) => {
  //                         }).catch((error) => {
  //                             // reject(error)
  //                             responseObj.status = errorCodes.SYNTAX_ERROR
  //                             responseObj.message = error.error_description
  //                             responseObj.data = {}
  //                             response.send(responseObj)
  //                         })
  //                     responseObj.status = errorCodes.SUCCESS
  //                     responseObj.message = language.getMessage('SUCCESS')
  //                     responseObj.data = data
  //                     response.send(responseObj)
  //                 }).catch(function (error) {
  //                     responseObj.status = errorCodes.SYNTAX_ERROR
  //                     responseObj.message = error.message
  //                     responseObj.data = {}
  //                     response.send(responseObj)
  //                 })
  //             }).catch((error) => {
  //                 responseObj.status = errorCodes.SYNTAX_ERROR
  //                 responseObj.message = error.message
  //                 responseObj.data = {}
  //                 response.send(responseObj)
  //             })
  //         } else {
  //             // Shop domain is not found in params then simply signup
  //             userRepository.createUser(params).then((userData) => {
  //                 const data = {}
  //                 data.authtoken = createJWT(userData._id)
  //                 data.user = userData

  //                 responseObj.status = errorCodes.SUCCESS
  //                 responseObj.message = language.getMessage('SUCCESS')
  //                 responseObj.data = data
  //                 response.send(responseObj)
  //             }).catch(function (error) {
  //                 responseObj.status = errorCodes.SYNTAX_ERROR
  //                 responseObj.message = error
  //                 responseObj.data = {}
  //                 response.send(responseObj)
  //             })
  //         }
  //     } else {
  //         responseObj.status = errorCodes.BAD_REQUEST
  //         responseObj.message = language.getMessage('EMAIL_ALREADY_EXISTS')
  //         responseObj.data = {}
  //         response.send(responseObj)
  //     }
  // })
