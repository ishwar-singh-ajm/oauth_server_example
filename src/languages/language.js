let language = ''
let langMessage

exports.init = function (lang) {
    language = lang
    const langPath = './' + language + '.json'
    langMessage = require(langPath)
    return this
}

exports.getMessage = function (messageCode) {
    return langMessage[messageCode]
}
