// Load Model
// const AuthModel = require('./../models/auth.model')
const { UserModel } = require('./../models/User.model')

// const { AppReview } = require('../models');
// class UserRepository {}
exports.createUser = async (params) => {
    return new Promise((resolve, reject) => {
        UserModel
            .create(params)
            .then((data) => { resolve(data) })
            .catch((error) => { reject(error) })
    })
}

exports.updateUser = async (where = {}, updateParams) => {
    return new Promise((resolve, reject) => {
        UserModel
            .updateMany(where, updateParams)
            .then((data) => { resolve(data) })
            .catch((error) => { reject(error) })
    })
}

exports.findAllUser = async (where = {}) => {
    return new Promise((resolve, reject) => {
        UserModel
            .findAll({ where })
            .then((data) => {
                resolve(data)
            })
            .catch((error) => {
                reject(error)
            })
    })
}

exports.listUser = async (where = {}) => {
    return new Promise((resolve, reject) => {
        UserModel
            .find(where)
            .then((data) => {
                resolve(data)
            })
            .catch((error) => {
                reject(error)
            })
    })
}

exports.findUser = async (where = {}) => {
    return new Promise((resolve, reject) => {
        UserModel
            .findOne(where)
            .then((data) => {
                resolve(data)
            })
            .catch((error) => {
                reject(error)
            })
    })
}

exports.deleteUser = async (params) => {
    return new Promise((resolve, reject) => {
        const where = { _id: params._id }
        UserModel
            .deleteOne(where)
            .then((data) => { resolve(data) })
            .catch((error) => { reject(error) })
    })
}