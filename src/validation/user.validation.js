
const { check } = require('express-validator')

/**
 * This function check product saveable data is valid or no
 */
exports.userSaveValidation = () => {
    return [
        check('title').exists().withMessage('Title field missing').trim().not().isEmpty().withMessage('Provide user title'),
        check('userType').exists().withMessage('user type is missing').trim().not().isEmpty().withMessage('Provide user type'),
        check('coverImage').exists().withMessage('user image is missing')
    ]
}
