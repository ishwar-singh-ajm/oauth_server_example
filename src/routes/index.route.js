const express = require('express')
const router = express.Router()

// Authorization Middleware
const AuthorizationMiddleware = require('./../middlewares/authorizations.middleware')

// Controller
const AuthController = require('./../controllers/auth.controller')
// // const ProductValidation = require('../validation/product.validation')

// Routes
router.post('/login', AuthController.login)
router.post('/register', AuthController.register)

// 404 API not found
router.get('*', function (req, res) {
    const body = { message: '404 API not found' }
    res.status(404).send(body)
})

module.exports = router
